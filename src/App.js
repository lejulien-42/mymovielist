import React from 'react'
import { useState, useEffect } from 'react';
import Movie from "./Movie"
import './App.css'


// get omdbapi link + private key via env vars
const API_URL = process.env.REACT_APP_MOVIE_API;



const App = () => {
	const [movies, setMovies] = useState([]);
	const [search, setSearch] = useState('');

	const searchMovies = async (title) => {
		const response = await fetch(`${API_URL}&s=${title}`);
		const data = await response.json();

		setMovies(data.Search);
	}

	const handleEnter = (event) => {
		if (event.key === "Enter") {
			searchMovies(search);
		}
	}

	useEffect(()=> {
		searchMovies('Deadpool');
	}, [])

	return (
		<div className='App'>
			<h1 class='SiteTitle'>MyMovieList</h1>
			<div className='search'>
				<input
					id="searchBar"
					placeholder='search for movies'
					value={search}
					onChange={(e) => {setSearch(e.target.value)}}
					onKeyDown={handleEnter}
				/>
				<button id="searchBtn" onClick={() => {searchMovies(search)}}>go</button>
			</div>
			{
				movies?.length > 0 ? (
					<div className='container'>
						{
							movies.map((movie) => (<Movie infos={movie}/>))
						}
					</div>
				) : (
					<div className='container'>
						<div className='empty'>
							<p>No such movie</p>
						</div>
					</div>	
				)
			}
		</div>
	);
}

export default App;
