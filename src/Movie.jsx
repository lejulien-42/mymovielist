import React from "react";

const Movie = ({ infos }) => {

    return (

        <div className='movie'>
            <div className='year'>
               <p>{infos.Year}</p>
            </div>
            <div>
                <img className='Poster' src={
					infos.Poster !== 'N/A' ? infos.Poster : 'https://via.placeholder.com/400'
				} alt={infos.Title} />
            </div>
            <div>
                <span>{infos.Type}</span>
                <h3>{infos.Title}</h3>
            </div>
        </div>
    );
};

export default Movie;
